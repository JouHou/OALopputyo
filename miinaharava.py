"""
Yksinkertainen miinaharava-peli, joka käyttää grafiikkakirjastonaan haravastoa (@author Mika Oja).
Haravasto on rakennettu pygletin päälle, joten pelataksesi peliä tarvitset pygletin. Sen voi 
asentaa komennolla "pip install pyglet".

@author Juho Holmi.

Pelissä on tekstimuotoinen alkuvalikko, mutta itse peli on graafisessa muodossa.
Alussa voit valita, haluatko pelata,katsella tilastoja tai lopettaa pelin. Jos
valitset pelaamisen, valitse sen jälkeen taso ja jos valitset edelleen omavalintaisen tason,
syötä kentän leveys, korkeus ja miinojen lukumäärä kokonaislukuina. Kun peli on päättynyt, pääset
takaisin valikkoon sulkemalla peli-ikkunan.
"""

import random
import time
import sys
import csv
import haravasto as h

TILA = {
    "kentta": [],
    "taso": (),
    "liput": [],
    "paljastamattomat": [],
    "miinat": [],
    "lopputulos": None,
    "aloitusaika": None,
    "kesto": None,
    "siirrot": None
}

GRAFIIKKA = {
    "ikkuna": None,
    "tausta": None,
    "taustavari": (127, 127, 127, 127),
    "puskuri": None,
    "spritet": [],
    "kuvat": {}
}

TASOT = {
    "a": (9, 9, 10),  # Aloittelija: 9x9 ruudukko, 10 miinaa #
    "k": (16, 16, 40),  # Keskitaso: 16x16 ruudukko, 40 miinaa #
    "e": (16, 30, 99),  # Ex-pertti: 30x16 ruudukko, 99 miinaa #
    "o": ()
}


def miinoita(vapaat_ruudut):
    """ Asettaa kentällä N kpl miinoja satunnaisiin paikkoihin. """
    for i in range(TILA["taso"][2]):
        x, y = random.choice(vapaat_ruudut)
        TILA["kentta"][y][x] = "X"
        TILA["miinat"][y][x] = True
        vapaat_ruudut.remove((x, y))


def numeroita(vapaat_ruudut):
    """ Laskee kunkin neliön viereiset miinat ja asettaa numerot. """
    for ruutu in vapaat_ruudut:
        miinoja = laske_miinat((ruutu[0]), ruutu[1])
        if miinoja != 0:
            TILA["kentta"][ruutu[1]][ruutu[0]] = miinoja


def tulvataytto(alkux, alkuy):
    """ Merkitsee kentällä olevat tuntemattomat alueet paljastetuiksi siten, että täyttö
    aloitetaan alkupisteestä. """
    tayttolista = [(alkux, alkuy)]
    while tayttolista:
        x, y = tayttolista.pop()
        TILA["paljastamattomat"][y][x] = False
        laske_tyhjat(x, y, tayttolista)


def laske_tyhjat(x, y, tayttolista):
    """ Apufunktio tulvatäytölle. """
    for i in range(max(0, y - 1), min(len(TILA["kentta"][0]), y + 2)):
        for j in range(max(0, x - 1), min(len(TILA["kentta"][0]), x + 2)):
            if TILA["kentta"][i][j] == ' ':
                tayttolista.append((j, i))
                TILA["kentta"][i][j] = '0'
            elif TILA["kentta"][i][j] != 'X':
                TILA["paljastamattomat"][i][j] = False


def laske_miinat(x, y):
    """ Laskee annetulla kentällä yhden ruudun ympärillä olevat miinat ja palauttaa niiden
    lukumäärän. """
    miinat = 0
    for i in range(max(0, y - 1), min(len(TILA["kentta"]), y + 2)):
        for j in range(max(0, x - 1), min(len(TILA["kentta"][0]), x + 2)):
            if TILA["kentta"][i][j] == 'X':
                miinat += 1
    return miinat


def tarkista_lopputulos():
    """ Tarkistaa lopputuloksen. """
    # Miinat ja liput samoissa, muut paljastettu #
    if TILA["miinat"] == TILA["liput"] == TILA["paljastamattomat"]:
        TILA["lopputulos"] = "Voitto"
        end = time.time()
        TILA["kesto"] = round(end - TILA["aloitusaika"], 1)
        print("Voitit pelin! Aikaa kului: ", TILA["kesto"], "s")
        tilastoi("a")
    elif TILA["lopputulos"] == "Häviö":
        end = time.time()
        TILA["kesto"] = round(end - TILA["aloitusaika"], 1)
        print("Hävisit pelin! Aikaa kului: ", TILA["kesto"], "s")
        tilastoi("a")


def tilastoi(moodi):
    """ Tilastointi ja tilastojen luku. Tilastoi ajan, keston, siirrot, lopputuloksen sekä
    vaikeustason. """
    try:
        with open("tilastot.txt", moodi, newline='') as kohde:
            if moodi == "r":
                lukija = csv.reader(kohde)
                for i, rivi in enumerate(lukija):
                    if i == 0:
                        print("{:20}{:8}{:8}{:11}{:14}{:8}{:8}"
                              .format(rivi[0], rivi[1], rivi[2], rivi[3], rivi[4],
                                      rivi[5], rivi[6]))
                    else:
                        print("{:21}{:8}{:8}{:11}{:14}{:8}{:8}"
                              .format(rivi[0], rivi[1]+" s", rivi[2], rivi[3], rivi[5],
                                      rivi[4], rivi[6]))
            elif moodi == "a":
                kirjoittaja = csv.writer(kohde)
                kirjoittaja.writerow([time.strftime("%d.%m.%Y %H:%M:%S",
                                                    time.localtime(TILA["aloitusaika"])),
                                      TILA["kesto"], TILA["siirrot"], TILA["lopputulos"],
                                      TILA["taso"][0], TILA["taso"][1], TILA["taso"][2]])
    except IOError:
        print("Tiedoston lukemisessa tai kirjoittamisessa tapahtui virhe.")


def piirra_kentta():
    """ Käsittelijäfunktio, joka piirtää miinakentän. """
    h.tyhjaa_ikkuna()
    h.piirra_tausta()
    h.aloita_ruutujen_piirto()
    for y in range(len(TILA["kentta"])):
        for x in range(len(TILA["kentta"][y])):
            if TILA["paljastamattomat"][y][x] and not TILA["liput"][y][x]:
                h.lisaa_piirrettava_ruutu(" ", 40 * x, 40 * y)
            elif TILA["liput"][y][x]:
                h.lisaa_piirrettava_ruutu("f", 40 * x, 40 * y)
            else:
                h.lisaa_piirrettava_ruutu(TILA["kentta"][y][x], 40 * x, 40 * y)
    h.piirra_ruudut()
    h.piirra_tekstia("MIINAHARAVA", 5, (TILA["taso"][0] * 40 + 40), (255, 255, 255, 255),
                     "verdana", 18)
    if TILA["lopputulos"] == "Voitto":
        h.piirra_tekstia("VOITTO! Aika: {}".format(TILA["kesto"]), 5, TILA["taso"][0] * 40,
                         (random.randint(0, 256),
                          random.randint(0, 256),
                          random.randint(0, 256),
                          random.randint(0, 256)),
                         "serif", 15)
    elif TILA["lopputulos"] == "Häviö":
        h.piirra_tekstia("MIINA! Aika: {}".format(TILA["kesto"]), 5, TILA["taso"][0] * 40,
                         (255, 255, 255, 255), "serif", 15)


def kasittele_hiiri(x, y, painike, nappaimet):
    """ Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä. """
    xruutu = int(x/40)
    yruutu = int(y/40)
    maxy = len(TILA["kentta"]) - 1

    if yruutu > maxy or TILA["lopputulos"] == "Häviö" or TILA["lopputulos"] == "Voitto":
        pass  # Älä tee mitään, jos klikataan kentän ulkopuolelle tai peli on jo päättynyt #
    else:
        if painike == h.HIIRI_VASEN:  # Ruudun paljastaminen #
            TILA["siirrot"] += 1
            # Jos paljastetussa ruudussa on miina #
            if TILA["kentta"][yruutu][xruutu] == "X" and not TILA["liput"][yruutu][xruutu]:
                paljasta_kentta()
                TILA["lopputulos"] = "Häviö"
                # Jos paljastetussa ruudussa ei ole miinaa #
            elif TILA["kentta"][yruutu][xruutu] != "X" and not TILA["liput"][yruutu][xruutu]:
                # Jos ruutu on tyhjä, kutsutaan tulvatäyttöä #
                if TILA["kentta"][yruutu][xruutu] == " ":
                    tulvataytto(xruutu, yruutu)
                TILA["paljastamattomat"][yruutu][xruutu] = False
        # Liput (paljastettuun ei lippua) #
        elif painike == h.HIIRI_OIKEA and TILA["paljastamattomat"][yruutu][xruutu]:
            TILA["siirrot"] += 1
            if not TILA["liput"][yruutu][xruutu]:  # Lipun asettaminen
                TILA["liput"][yruutu][xruutu] = True
            elif TILA["liput"][yruutu][xruutu]:  # Lipun poistaminen
                TILA["liput"][yruutu][xruutu] = False

        tarkista_lopputulos()
    piirra_kentta()


def paljasta_kentta():
    """ Paljastaa kentän häviön jälkeen """
    for sarake in range(TILA["taso"][0]):
        for rivi in range(TILA["taso"][1]):
            TILA["paljastamattomat"][sarake][rivi] = False
            TILA["liput"][sarake][rivi] = False
            if TILA["kentta"][sarake][rivi] == " ":
                TILA["kentta"][sarake][rivi] = "0"


def valikko():
    """ Valikko """
    print("\nTervetuloa pelaamaan Miinaharavaa!\n")

    while True:
        valinta = input("Valitse: (P)elaa, (K)atsele tilastoja tai (L)opeta > ")

        if valinta.lower() == "l":
            sys.exit()
        elif valinta.lower() == "p":
            print("\nTasot: \n{:15} (9x9 ruudukko, 10 miinaa)".format("Aloittelija"))
            print("{:15} (16x16 ruudukko, 40 miinaa)".format("Keskitaso"))
            print("{:15} (16x30 ruudukko, 99 miinaa)".format("Ex-pertti"))
            print("{:15} (Sivun pituus vapaa, miinojen maksimilukumäärä L x K)\n"
                  .format("Omavalintainen"))
            while True:
                try:
                    taso = input("Valitse taso: (A)loittelija, (K)eskitaso, (E)xpertti "
                                 "tai (O)mavalintainen > ")
                    TILA["taso"] = TASOT[taso.lower()]
                except KeyError:
                    print("Valintasi oli virheellinen")
                else:
                    break
            if taso == "o":
                while True:
                    try:
                        leveys = int(input("\nAnna leveys > "))
                        korkeus = int(input("Anna korkeus > "))
                        miinojenlkm = int(input("Anna miinojen lukumäärä > "))
                        while miinojenlkm > leveys * korkeus:
                            print("Miinoja ei voi olla enemmän kuin ruutuja!")
                            miinojenlkm = int(input("Anna miinojen lukumäärä > "))
                    except ValueError:
                        print("Anna arvot kokonaislukuina, kiitos.")
                    else:
                        TILA["taso"] = korkeus, leveys, miinojenlkm
                        break
            return True  # Mennään peliin jos valinta on (P)elaa #
        elif valinta.lower() == "k":
            print("\n" + "*"*76 + "\n{:^76}\n".format("Tilastot") + "*"*76)
            tilastoi("r")
            return False  # Ei mennä peliin jos valinta on (K)atsele tilastoja #
        else:
            print("Valintaasi vastaavaa toimintoa ei löytynyt. Yritä uudelleen.")


def main():
    """ Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän. """
    h.lataa_kuvat("spritet")
    h.luo_ikkuna(40 * TILA["taso"][1], 40 * (TILA["taso"][0] + 2), GRAFIIKKA["taustavari"])
    h.aseta_piirto_kasittelija(piirra_kentta)
    h.aseta_hiiri_kasittelija(kasittele_hiiri)
    TILA["aloitusaika"] = time.time()
    h.aloita()


if __name__ == "__main__":
    while True:
        if valikko():  # Jos valikko palauttaa True:n, mennään peliin #

            # Kentän, lippujen, paljastettujen, miinojen ja vapaiden ruutujen alustus #
            KENTTA, LIPUT, PALJASTAMATTOMAT, MIINAT, VAPAAT_RUUDUT = [], [], [], [], []
            for sarak in range(TILA["taso"][0]):
                KENTTA.append([])
                LIPUT.append([])
                PALJASTAMATTOMAT.append([])
                MIINAT.append([])
                for riv in range(TILA["taso"][1]):
                    KENTTA[-1].append(" ")
                    LIPUT[-1].append(False)
                    PALJASTAMATTOMAT[-1].append(True)
                    MIINAT[-1].append(False)
                    VAPAAT_RUUDUT.append((riv, sarak))

            # Alusta pelin tila uudestaan, miinoita, lisää numerolaatat ja aloita peli #
            TILA["kentta"] = KENTTA
            TILA["liput"] = LIPUT
            TILA["paljastamattomat"] = PALJASTAMATTOMAT
            TILA["miinat"] = MIINAT
            TILA["lopputulos"] = None
            TILA["kesto"] = None
            TILA["siirrot"] = 0
            miinoita(VAPAAT_RUUDUT)
            numeroita(VAPAAT_RUUDUT)
            main()
