Yksinkertainen miinaharava-peli, joka käyttää grafiikkakirjastonaan Mika Ojan luomaa haravasto-kirjastoa. Haravasto on rakennettu pygletin päälle, joten pelataksesi peliä tarvitset pygletin. Sen voi asentaa komennolla "pip install pyglet".

Pelissä on tekstimuotoinen alkuvalikko, mutta itse peli on graafisessa muodossa.
Alussa voit valita, haluatko pelata,katsella tilastoja tai lopettaa pelin. Jos
valitset pelaamisen, valitse sen jälkeen taso ja jos valitset edelleen omavalintaisen tason,
syötä kentän leveys, korkeus ja miinojen lukumäärä kokonaislukuina. Kun peli on päättynyt, pääset
takaisin valikkoon sulkemalla peli-ikkunan.
